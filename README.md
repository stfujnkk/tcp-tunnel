# tcp隧道

#### 介绍

python实现的简易tcp隧道,用于 ssh 内网穿透

也可以用于其他tcp流量的转发,更改一下端口号即可

#### 使用说明

1.  把 server.py 和 tunnel.py 上传到服务器
2.  把 client.py 和 tunnel.py 留在本地
3.  先在服务器运行 server.py,然后在本地运行 client.py
4.  用 `ssh 用户名@服务器ip -p  端口号`命令登录内网电脑

这个项目写的比较简陋，我还有一个[项目](https://github.com/stfujnkk/zomboid-forward)实现的更好。可以用于游戏服务器的内网穿透，如僵尸毁灭工程。

