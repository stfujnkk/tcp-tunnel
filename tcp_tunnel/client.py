#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from socket import socket, AF_INET, SOCK_STREAM

from tunnel import bridger

while True:
    s1 = socket(AF_INET, SOCK_STREAM)
    s1.connect(('47.112.143.9', 7766))
    s1.settimeout(2)
    ok = s1.recv(7)
    if ok != b'server?':
        break
    s1.send(b'ok')
    s1.settimeout(None)
    ok = s1.recv(2)
    if ok != b'ok':
        break
    s2 = socket(AF_INET, SOCK_STREAM)
    s2.connect(('127.0.0.1', 22))
    bridger(s1, s2).start()

