#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from socket import socket, AF_INET, SOCK_STREAM
from tunnel import bridger

s1 = socket(AF_INET, SOCK_STREAM)
s1.bind(('0.0.0.0', 7766))
s1.listen(0)
while True:
    s2, _ = s1.accept()
    s2.settimeout(1)
    s2.send(b'server?')
    data = s2.recv(2)
    if data != b'ok':
        s2.close()
        continue
    s2.settimeout(None)
    s3, _ = s1.accept()
    s2.send(b'ok')
    bridger(s2, s3).start()

